<?php

namespace App;

use App\Movie;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    /**
    * The movies that belong to the actor.
    */

    public function movies()
    {
        return $this->belongsToMany(Movie::class, 'actor_movie')->withPivot('acting_desc');
    }

    public static function getMoviesArchive()
    {
    	return Movie::selectRaw('year(release_date) as year,monthname(release_date) as month,count(*) as no_of_movie')
            ->groupBy('year', 'month')
            ->orderByRaw('min(release_date) desc')
            ->get()
            ->toArray();
    }
}
