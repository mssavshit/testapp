<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Gate;

class CheckUserRights
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        if($request->user()->is_admin || Gate::allows('check-rights'))
        {
            return $next($request);    
        }
        else
        {
            return response()->json('You not have enough rights to perform this action.');
        }
    }
}
