<?php

namespace App\Http\ViewComposers;

use App\Movie;
use App\ActorMovie;
use Gate;

class SidebarComposer
{
    public function compose($view)
    {
    	if(auth()->user()->is_admin || Gate::allows('check-rights'))
    	{
    		$view->with('releasedDate',\App\Movie::released());	
    	}
    	else
    	{
            $view->with('moviesarchive',\App\User::getMoviesArchive());
    	}
        
    }
}