<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UserRegistration;
use App\Repositories\UserRepository; //Services
class RegistrationController extends Controller
{
    public function __construct()
    {
    	$this->middleware('guest',['except'=>'store']);
    }

    public function create(){
    	return view('registration.register');
    }

    public function store(UserRegistration $request,UserRepository $users)
    {
        $user = $users->add();
    	auth()->login($user);
        session()->flash('message','Thanks For the registration');
    	return redirect('/');
    }


}
