<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;
use Auth;
class AuthenticationController extends Controller
{
    //

    public function __construct()
    {
    	$this->middleware('guest',['except'=>'destroy']);
    }
     public function create(){
		return view('authentication.login');
    }
    public function store()
    { 
    	$this->validate(request(),[
    			'email'=>'required|email',
    			'password'=>'required'
    		]);
    	
    	if(! Auth::attempt(request(['email','password']))){

    			return back()->withErrors([
    					'message'=>'Credential Not matched.'
    				]);
    	}
    	return redirect('/');
    }
    public function destroy()
    {
    	auth()->logout();
    	return redirect('/login');
    }
}
