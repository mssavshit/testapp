<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Redirector;
use Gate;

use App\Movie;
use App\User;
use Illuminate\Http\Request;
use App\Repositories\MoviesRepository;
use App\Http\Requests\MoviesRequest;
use App\Events\ActorAddInMovie;

use Auth;
use App\CustomFacade\Moviedetail;

class MoviesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }

    public function index(MoviesRepository $movies)
    {
        $movies = $movies->all();
        return view('movies.index', ['movies' => $movies]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $actors = User::all();
        return view('movies.create', compact('actors'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MoviesRequest $request, MoviesRepository $movies)
    {
        $movie = $movies->add();

        $actorsArr = $request->input('actors');

        if($movie->actors->contains($movie))
        {
            session()->flash('message','Movie added successfully');
            return Redirect::route('movie.create');
        }
        else
        {
            $movie->actors()->attach($actorsArr);    
        }
        
        // Send email to actor when it added in movie.
        event(new ActorAddInMovie($movie, $actorsArr));
        session()->flash('message','Movie added successfully');
        return redirect('movie/list');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($movie_id)
    {
        // Used facade to get movie details.
        $movieDetail = Moviedetail::getMovieDetails($movie_id);
        
        return view('movies.detail', compact('movieDetail'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function actorMovie(Request $request, MoviesRepository $movies)
    {
    	$actorMovies = $movies->actorWiseMovies($request);
    	return view('actor.index', compact('actorMovies'));
    }

    public function createActing($movie_id)
    {
    	$acting_desc = auth()->user()->movies->find($movie_id)->pivot->acting_desc;
    	return view('actor.acting', ['movie_id' => $movie_id, 'acting_desc' => $acting_desc]);
    }

    public function storeActing(Request $request)
    {
        $this->validate($request,[
                'acting_desc'=>'required'
        ]);

    	$movie_id = $request->input('movie_id');
    	$acting_desc = trim($request->input('acting_desc'));
    	
        //dd(['year'=>$request->year,'month'=>$request->month]);

    	$actor = auth()->user();
		$actor->movies()->updateExistingPivot($movie_id, ['acting_desc' => $acting_desc]);
    	return redirect()->route('actor_movies_list', ['year'=>$request->year,'month'=>$request->month]);
    }
}
