<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActorMovie extends Model
{
	protected $table = "actor_movie";
	public $timestamps = false;
	
    public function movie()
    {
        return $this->belongsToMany('App\Movie');
    }
}
