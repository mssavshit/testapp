<?php

namespace App\Listeners;

use App\Events\ActorAddInMovie;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmailNotificationToActor
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ActorAddInMovie  $event
     * @return void
     */
    public function handle(ActorAddInMovie $event)
    {
        // Code to send mail to all actors who are added in particular movie by admin.
    }
}
