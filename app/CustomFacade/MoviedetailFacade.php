<?php
	
namespace App\CustomFacade;

use Illuminate\Support\Facades\Facade;

class MoviedetailFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'moviedetail';
    }
}