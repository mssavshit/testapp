<?php

namespace App\CustomFacade;

use App\Movie;

class Moviedetail
{
    public static function getMovieDetails($movie_id)
    {
        $res = Movie::with('actors')
        ->where('id', $movie_id)
        ->get()
        ->toArray();

        return $res[0];
    }
}