<?php

namespace App\Providers;

use App\CustomFacade;

use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;

class MoviedetailServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        \App::bind('moviedetail'::class,function($movie_id){
            return new Moviedetail($movie_id);
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
