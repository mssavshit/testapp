<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Movie extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'movie_name', 'release_date',
    ];

    public function actors()
    {
        return $this->belongsToMany(User::class, 'actor_movie')->withPivot('acting_desc');
    }

    public static function released()
    {
        return static::selectRaw('year(release_date) as year,monthname(release_date) as month,count(*) as no_of_movie')
            ->groupBy('year', 'month')
            ->orderByRaw('min(release_date) desc')
            ->get()
            ->toArray();
    }

    public static function getAllMonthWiseMovies()
    {
        if(count(request(['year', 'month'])) == 2)
        {
        	$month = date("m", strtotime(request(['month'][0])));
	        $year = request(['year'][0]);
	        
	        //return Movie::latest()->Filter(request(['year', 'month']))->get();
	        return Movie::latest()
	                        ->whereMonth('release_date', '=', $month)
	                        ->whereYear('release_date', '=', $year)
	                        ->get();    
        }
        else
        {
		    //return Movie::latest()->Filter(request(['year', 'month']))->get();
		    return Movie::latest()->get();
        }
        
    }
}
