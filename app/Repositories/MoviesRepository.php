<?php

namespace App\Repositories;
 
use App\Movie;
use App\User;
use Auth;

class MoviesRepository{
	
	protected $movie;
    public function __construct(){
        $this->movie = new Movie();
    }

    public function publish($movie){
       return $this->movie->publish($movie);
    }

    public function add(){
        return $movie = Movie::create([
            'movie_name'=>request('movie_name'),
            'release_date'=>request('release_date'),
         ]);
    }

    public function all(){
       return Movie::getAllMonthWiseMovies();
    }

    public function show($movie_id){
       return Movie::getAllMonthWiseMovies();
    }

    public function actorWiseMovies($request)
    {
        $actor = Auth::user();
        $id = $actor->id;

        $year  = $request->year;
        $month = date_parse($request->month)['month'];

        $res = User::with(['movies' => function($q) use ($year, $month){
                $q->whereYear('release_date', '=', $year);
                $q->whereMonth('release_date', '=', $month);
        }])
        ->where('id', $id)
        ->get()
        ->toArray();
        $actorMovies = $res[0];

        return $actorMovies;
    }
}