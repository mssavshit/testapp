-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.3.15-MariaDB-log - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for user_addr
CREATE DATABASE IF NOT EXISTS `user_addr` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `user_addr`;

-- Dumping structure for table user_addr.actor_movie
CREATE TABLE IF NOT EXISTS `actor_movie` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `actor_id` int(10) unsigned NOT NULL,
  `movie_id` int(10) unsigned NOT NULL,
  `actor_role_desc` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table user_addr.actor_movie: ~0 rows (approximately)
/*!40000 ALTER TABLE `actor_movie` DISABLE KEYS */;
/*!40000 ALTER TABLE `actor_movie` ENABLE KEYS */;

-- Dumping structure for table user_addr.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table user_addr.migrations: ~6 rows (approximately)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2019_08_18_140715_add_fields_to_users', 2),
	(5, '2019_08_18_143111_create_movie_table', 3),
	(6, '2019_08_18_180651_create_actor_movie_table', 4),
	(7, '2019_08_18_181625_create_actor_movie_table', 5);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table user_addr.movies
CREATE TABLE IF NOT EXISTS `movies` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `movie_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `release_date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table user_addr.movies: ~5 rows (approximately)
/*!40000 ALTER TABLE `movies` DISABLE KEYS */;
INSERT INTO `movies` (`id`, `movie_name`, `release_date`, `created_at`, `updated_at`) VALUES
	(1, 'Movie 1', '2019-08-18', '2019-08-18 20:37:53', '2019-08-18 20:37:53'),
	(2, 'Movie 2', '2019-07-01', '2019-08-18 17:14:47', '2019-08-18 17:14:47'),
	(3, 'Movie 3', '2019-06-10', '2019-08-18 17:29:47', '2019-08-18 17:29:47'),
	(4, 'Movie 4', '2019-06-20', '2019-08-18 17:30:55', '2019-08-18 17:30:55'),
	(5, 'Movie 5', '2019-05-25', '2019-08-18 17:32:39', '2019-08-18 17:32:39');
/*!40000 ALTER TABLE `movies` ENABLE KEYS */;

-- Dumping structure for table user_addr.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table user_addr.password_resets: ~0 rows (approximately)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Dumping structure for table user_addr.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_admin` int(11) NOT NULL DEFAULT 0,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table user_addr.users: ~4 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `is_admin`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 'test', 'test@gmail.com', 0, NULL, '$2y$10$vZENCy8txiY9N84bQ.pzdOWCXem2ToKv/NpwOsc7riQO7RhsIzJHC', NULL, '2019-08-18 06:44:03', '2019-08-18 06:44:03'),
	(2, 'test1', 'test1@gmail.com', 0, NULL, '$2y$10$4zhwgXcU0Xu5dLSeFMdsxOHmq3P6/S1UFKBIRvNBapupKYy5txSR6', NULL, '2019-08-18 08:14:25', '2019-08-18 08:14:25'),
	(3, 'test2', 'test2@gmail.com', 0, NULL, '$2y$10$z5nvmZorNsdKGNSb5Z7v5OWbV2Kh8b9a/Yd4cwas.9vk3gWfZRdIi', NULL, '2019-08-18 13:46:09', '2019-08-18 13:46:09'),
	(4, 'Admin', 'admin@gmail.com', 1, '2019-08-18 23:17:21', '$2y$10$xiOu8DpCDttnkXG0Ynmn8.NyOhzA0KnRddxIqyUtt34aJD5d2.5T2', NULL, '2019-08-18 17:47:10', '2019-08-18 17:47:10');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
