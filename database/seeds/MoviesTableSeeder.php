<?php

use Illuminate\Database\Seeder;

class MoviesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Movie::truncate();
        DB::table('movies')->insert([
            'movie_name' => 'Student Of The Year 2',
            'release_date'=>'2019-05-10',
            'created_at'=>date("Y-m-d H:i:s"),
            'updated_at'=>date("Y-m-d H:i:s")
        ]);

        DB::table('movies')->insert([
            'movie_name' => 'India\'s Most Wanted',
            'release_date'=>'2019-05-24',
            'created_at'=>date("Y-m-d H:i:s"),
            'updated_at'=>date("Y-m-d H:i:s")
        ]);

        DB::table('movies')->insert([
            'movie_name' => 'Bharat',
            'release_date'=>'2019-06-05',
            'created_at'=>date("Y-m-d H:i:s"),
            'updated_at'=>date("Y-m-d H:i:s")
        ]);

        DB::table('movies')->insert([
            'movie_name' => 'Kabir Singh',
            'release_date'=>'2019-06-21',
            'created_at'=>date("Y-m-d H:i:s"),
            'updated_at'=>date("Y-m-d H:i:s")
        ]);

        DB::table('movies')->insert([
            'movie_name' => 'Mental Hai Kya',
            'release_date'=>'2019-06-21',
            'created_at'=>date("Y-m-d H:i:s"),
            'updated_at'=>date("Y-m-d H:i:s")
        ]);

        DB::table('movies')->insert([
            'movie_name' => 'Drive ',
            'release_date'=>'2019-06-28',
            'created_at'=>date("Y-m-d H:i:s"),
            'updated_at'=>date("Y-m-d H:i:s")
        ]);

        DB::table('movies')->insert([
            'movie_name' => 'Jabariya Jodi',
            'release_date'=>'2019-07-12',
            'created_at'=>date("Y-m-d H:i:s"),
            'updated_at'=>date("Y-m-d H:i:s")
        ]);

        DB::table('movies')->insert([
            'movie_name' => 'Arjun Patiala',
            'release_date'=>'2019-07-19',
            'created_at'=>date("Y-m-d H:i:s"),
            'updated_at'=>date("Y-m-d H:i:s")
        ]);

        DB::table('movies')->insert([
            'movie_name' => 'Super 30',
            'release_date'=>'2019-07-26',
            'created_at'=>date("Y-m-d H:i:s"),
            'updated_at'=>date("Y-m-d H:i:s")
        ]);

        DB::table('movies')->insert([
            'movie_name' => 'Batla House',
            'release_date'=>'2019-08-15',
            'created_at'=>date("Y-m-d H:i:s"),
            'updated_at'=>date("Y-m-d H:i:s")
        ]);

        DB::table('movies')->insert([
            'movie_name' => 'Mission Mangal',
            'release_date'=>'2019-08-15',
            'created_at'=>date("Y-m-d H:i:s"),
            'updated_at'=>date("Y-m-d H:i:s")
        ]);

        DB::table('movies')->insert([
            'movie_name' => 'Chhichhore',
            'release_date'=>'2019-08-30',
            'created_at'=>date("Y-m-d H:i:s"),
            'updated_at'=>date("Y-m-d H:i:s")
        ]);

        DB::table('movies')->insert([
            'movie_name' => 'Made in China',
            'release_date'=>'2019-08-30',
            'created_at'=>date("Y-m-d H:i:s"),
            'updated_at'=>date("Y-m-d H:i:s")
        ]);
    }
}
