<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\User::truncate();
        DB::table('users')->insert([
            'name' => 'Admin',
            'email'=>'admin@gmail.com',
            'password'=>bcrypt('admin12345'),
            'is_admin' => 1,
            'created_at'=>date("Y-m-d H:i:s"),
            'updated_at'=>date("Y-m-d H:i:s")
        ]);

        DB::table('users')->insert([
            'name' => 'Amitabh Bachchan',
            'email'=>'actor1@gmail.com',
            'password'=>bcrypt('actor12345'),
            'created_at'=>date("Y-m-d H:i:s"),
            'updated_at'=>date("Y-m-d H:i:s")
        ]);

        DB::table('users')->insert([
            'name' => 'Salman Khan',
            'email'=>'actor2@gmail.com',
            'password'=>bcrypt('actor12345'),
            'created_at'=>date("Y-m-d H:i:s"),
            'updated_at'=>date("Y-m-d H:i:s")
        ]);

        DB::table('users')->insert([
            'name' => 'Shah Rukh Khan',
            'email'=>'actor3@gmail.com',
            'password'=>bcrypt('actor12345'),
            'created_at'=>date("Y-m-d H:i:s"),
            'updated_at'=>date("Y-m-d H:i:s")
        ]);

        DB::table('users')->insert([
            'name' => 'Aamir Khan',
            'email'=>'actor4@gmail.com',
            'password'=>bcrypt('actor12345'),
            'created_at'=>date("Y-m-d H:i:s"),
            'updated_at'=>date("Y-m-d H:i:s")
        ]);

        DB::table('users')->insert([
            'name' => 'Akshay Kumar',
            'email'=>'actor5@gmail.com',
            'password'=>bcrypt('actor12345'),
            'created_at'=>date("Y-m-d H:i:s"),
            'updated_at'=>date("Y-m-d H:i:s")
        ]);

        DB::table('users')->insert([
            'name' => 'Ajay Devgn',
            'email'=>'actor6@gmail.com',
            'password'=>bcrypt('actor12345'),
            'created_at'=>date("Y-m-d H:i:s"),
            'updated_at'=>date("Y-m-d H:i:s")
        ]);

        DB::table('users')->insert([
            'name' => 'Sanjay Dutt',
            'email'=>'actor7@gmail.com',
            'password'=>bcrypt('actor12345'),
            'created_at'=>date("Y-m-d H:i:s"),
            'updated_at'=>date("Y-m-d H:i:s")
        ]);

        DB::table('users')->insert([
            'name' => 'Hrithik Roshan',
            'email'=>'actor8@gmail.com',
            'password'=>bcrypt('actor12345'),
            'created_at'=>date("Y-m-d H:i:s"),
            'updated_at'=>date("Y-m-d H:i:s")
        ]);

        DB::table('users')->insert([
            'name' => 'Jackie Shroff',
            'email'=>'actor9@gmail.com',
            'password'=>bcrypt('actor12345'),
            'created_at'=>date("Y-m-d H:i:s"),
            'updated_at'=>date("Y-m-d H:i:s")
        ]);

        DB::table('users')->insert([
            'name' => 'Ranbir Kapoor',
            'email'=>'actor10@gmail.com',
            'password'=>bcrypt('actor12345'),
            'created_at'=>date("Y-m-d H:i:s"),
            'updated_at'=>date("Y-m-d H:i:s")
        ]);

        DB::table('users')->insert([
            'name' => 'Rajinikanth',
            'email'=>'actor11@gmail.com',
            'password'=>bcrypt('actor12345'),
            'created_at'=>date("Y-m-d H:i:s"),
            'updated_at'=>date("Y-m-d H:i:s")
        ]);

        DB::table('users')->insert([
            'name' => 'Prabhas',
            'email'=>'actor12@gmail.com',
            'password'=>bcrypt('actor12345'),
            'created_at'=>date("Y-m-d H:i:s"),
            'updated_at'=>date("Y-m-d H:i:s")
        ]);

        DB::table('users')->insert([
            'name' => 'Mahesh Babu',
            'email'=>'actor13@gmail.com',
            'password'=>bcrypt('actor12345'),
            'created_at'=>date("Y-m-d H:i:s"),
            'updated_at'=>date("Y-m-d H:i:s")
        ]);

        DB::table('users')->insert([
            'name' => 'Ram Charan',
            'email'=>'actor14@gmail.com',
            'password'=>bcrypt('actor12345'),
            'created_at'=>date("Y-m-d H:i:s"),
            'updated_at'=>date("Y-m-d H:i:s")
        ]);

        DB::table('users')->insert([
            'name' => 'Anushka Shetty',
            'email'=>'actor@gmail.com',
            'password'=>bcrypt('actor12345'),
            'created_at'=>date("Y-m-d H:i:s"),
            'updated_at'=>date("Y-m-d H:i:s")
        ]);

        DB::table('users')->insert([
            'name' => 'Kajal Aggarwal',
            'email'=>'actor15@gmail.com',
            'password'=>bcrypt('actor12345'),
            'created_at'=>date("Y-m-d H:i:s"),
            'updated_at'=>date("Y-m-d H:i:s")
        ]);
        DB::table('users')->insert([
            'name' => 'Shruti Haasan',
            'email'=>'actor16@gmail.com',
            'password'=>bcrypt('actor12345'),
            'created_at'=>date("Y-m-d H:i:s"),
            'updated_at'=>date("Y-m-d H:i:s")
        ]);
        DB::table('users')->insert([
            'name' => 'Tamannaah Bhatia',
            'email'=>'actor17@gmail.com',
            'password'=>bcrypt('actor12345'),
            'created_at'=>date("Y-m-d H:i:s"),
            'updated_at'=>date("Y-m-d H:i:s")
        ]);
        DB::table('users')->insert([
            'name' => 'Priyanka Chopra',
            'email'=>'actor18@gmail.com',
            'password'=>bcrypt('actor12345'),
            'created_at'=>date("Y-m-d H:i:s"),
            'updated_at'=>date("Y-m-d H:i:s")
        ]);
        DB::table('users')->insert([
            'name' => 'Vidya Balan',
            'email'=>'actor19@gmail.com',
            'password'=>bcrypt('actor12345'),
            'created_at'=>date("Y-m-d H:i:s"),
            'updated_at'=>date("Y-m-d H:i:s")
        ]);
        DB::table('users')->insert([
            'name' => 'Deepika Padukone',
            'email'=>'actor20@gmail.com',
            'password'=>bcrypt('actor12345'),
            'created_at'=>date("Y-m-d H:i:s"),
            'updated_at'=>date("Y-m-d H:i:s")
        ]);

        DB::table('users')->insert([
            'name' => 'Kangana Ranaut',
            'email'=>'actor21@gmail.com',
            'password'=>bcrypt('actor12345'),
            'created_at'=>date("Y-m-d H:i:s"),
            'updated_at'=>date("Y-m-d H:i:s")
        ]);
        DB::table('users')->insert([
            'name' => 'Katrina Kaif',
            'email'=>'actor22@gmail.com',
            'password'=>bcrypt('actor12345'),
            'created_at'=>date("Y-m-d H:i:s"),
            'updated_at'=>date("Y-m-d H:i:s")
        ]);
        DB::table('users')->insert([
            'name' => 'Alia Bhatt',
            'email'=>'actor23@gmail.com',
            'password'=>bcrypt('actor12345'),
            'created_at'=>date("Y-m-d H:i:s"),
            'updated_at'=>date("Y-m-d H:i:s")
        ]);
        DB::table('users')->insert([
            'name' => 'Anushka Sharma',
            'email'=>'actor24@gmail.com',
            'password'=>bcrypt('actor12345'),
            'created_at'=>date("Y-m-d H:i:s"),
            'updated_at'=>date("Y-m-d H:i:s")
        ]);
        DB::table('users')->insert([
            'name' => 'Boman Irani',
            'email'=>'actor25@gmail.com',
            'password'=>bcrypt('actor12345'),
            'created_at'=>date("Y-m-d H:i:s"),
            'updated_at'=>date("Y-m-d H:i:s")
        ]);
        DB::table('users')->insert([
            'name' => 'Dalip Tahil',
            'email'=>'actor26@gmail.com',
            'password'=>bcrypt('actor12345'),
            'created_at'=>date("Y-m-d H:i:s"),
            'updated_at'=>date("Y-m-d H:i:s")
        ]);
        DB::table('users')->insert([
            'name' => 'Irrfan Khan',
            'email'=>'actor27@gmail.com',
            'password'=>bcrypt('actor12345'),
            'created_at'=>date("Y-m-d H:i:s"),
            'updated_at'=>date("Y-m-d H:i:s")
        ]);
        DB::table('users')->insert([
            'name' => 'Paresh Rawal',
            'email'=>'actor28@gmail.com',
            'password'=>bcrypt('actor12345'),
            'created_at'=>date("Y-m-d H:i:s"),
            'updated_at'=>date("Y-m-d H:i:s")
        ]);
        DB::table('users')->insert([
            'name' => 'Prakash Raj',
            'email'=>'actor29@gmail.com',
            'password'=>bcrypt('actor12345'),
            'created_at'=>date("Y-m-d H:i:s"),
            'updated_at'=>date("Y-m-d H:i:s")
        ]);
        DB::table('users')->insert([
            'name' => 'Ronit Roy',
            'email'=>'actor30@gmail.com',
            'password'=>bcrypt('actor12345'),
            'created_at'=>date("Y-m-d H:i:s"),
            'updated_at'=>date("Y-m-d H:i:s")
        ]);

        DB::table('users')->insert([
            'name' => 'Sonu Sood',
            'email'=>'actor31@gmail.com',
            'password'=>bcrypt('actor12345'),
            'created_at'=>date("Y-m-d H:i:s"),
            'updated_at'=>date("Y-m-d H:i:s")
        ]);
    }
}
