<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\ActorMovie;
use Faker\Generator as Faker;

$factory->define(ActorMovie::class, function (Faker $faker) {
 	
 	$user_id = $faker->numberBetween(2,33);
	$movie_id = uniqueOrder($user_id, $faker); 	

    return [
        'user_id' => $user_id,
        'movie_id' => $movie_id,
        'acting_desc' => $faker->paragraph($nbSentences = 3, $variableNbSentences = true)
    ];
});

function uniqueOrder($user_id, $faker)
{
    do {
       	$movie_id = $faker->numberBetween(1,13);
    }
    while(ActorMovie::where(function($q) use ($user_id, $movie_id){
						    	$q->where('user_id', $user_id);
						    	$q->where('movie_id', $movie_id);	
							})->exists()
	);

  	return $movie_id;

}
