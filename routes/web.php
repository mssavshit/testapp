<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layouts.app');
})->name('home');
//Auth::routes();

Route::group(['middleware' => 'auth'], function () {

	Route::group(['prefix' => 'movie', 'middleware' => 'userrights'], function () {
		Route::get('/list',['as'=>'movies_list','uses'=>'MoviesController@index']);
		Route::get('/create',['as'=>'movies_create','uses'=>'MoviesController@create']);
		Route::post('',['as'=>'movies_store','uses'=>'MoviesController@store']);
	});

	Route::group(['prefix' => 'actor'], function () {
		Route::get('/movies',['as'=>'actor_movies_list','uses'=>'MoviesController@actorMovie']);
		Route::get('/acting/{id}',['as'=>'acting_create','uses'=>'MoviesController@createActing']);
		Route::post('/movie/store',['as'=>'acting_store','uses'=>'MoviesController@storeActing']);
		//Route::get('/actor/acting/edit/{id}',['as'=>'acting_edit','uses'=>'MoviesController@editActing']);
	});
	Route::get('movie/{id}',['as'=>'movies_show','uses'=>'MoviesController@show']);
});



Route::get('/register',['as'=>'register','uses'=>'RegistrationController@create']);
Route::post('/register',['as'=>'register_create','uses'=>'RegistrationController@store']);
Route::get('/login',['as'=>'session','uses'=>'AuthenticationController@create']);
Route::post('/login',['as'=>'login','uses'=>'AuthenticationController@store']);
Route::get('/logout',['as'=>'logout','uses'=>'AuthenticationController@destroy']);

//Route::get('/home', 'HomeController@index')->name('home');
