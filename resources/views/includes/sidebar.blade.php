    <div class="col-sm-3 blog-sidebar">
        
		<div class="card">
			<ul class="list-group list-group-flush">
				@if(isset($releasedDate) && count($releasedDate) > 0)
					<li class="list-group-item bg-info">
						<a class="text-white" href="JAVASCRIPT:void(0);">Released On</a>
					</li>
					@foreach($releasedDate as $reldt)
					<li class="list-group-item">
						<a href="{{ route('movies_list') }}?year={{ $reldt['year'] }}&month={{ $reldt['month'] }}">{{ $reldt['month'] }}-{{ $reldt['year'] }} ({{ $reldt['no_of_movie'] }})</a>
					</li>
					@endforeach
				@endif

				@if(isset($moviesarchive) && count($moviesarchive) > 0)
					<li class="list-group-item bg-info">
						<a class="text-white" href="JAVASCRIPT:void(0);">Act In Movie</a>
					</li>

					@foreach($moviesarchive as $reldt)
				
					<li class="list-group-item">
						<a href="{{ route('actor_movies_list') }}?year={{ $reldt['year'] }}&month={{ $reldt['month'] }}">{{ $reldt['month'] }}-{{ $reldt['year'] }} ({{ $reldt['no_of_movie'] }})</a>
					</li>
					@endforeach
				@endif
			</ul>
		</div>
    </div>
