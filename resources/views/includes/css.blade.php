<!-- Fonts -->
<link rel="dns-prefetch" href="//fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

<link rel="stylesheet" href="/css/bootstrap.min.css	">
<link rel="stylesheet" href="/css/app.css">
<link rel="stylesheet" href="/css/blog.css">

<!-- Styles -->
<link href="{{ asset('css/app.css') }}" rel="stylesheet">