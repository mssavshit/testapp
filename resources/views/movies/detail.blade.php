@extends('layouts.app')

@section('content')

        <div class="col-sm-9 main">
			
			<div class="jumbotron" style="background: #fff;">
			    <h3 class="h1-responsive section-title">Movie Detail</h3>
			   
		       
			    <hr class="my-2">
			   
				<div>
					<table class="table table-striped">
						<thead>
							<tr>
								<th>Movie Name</th>
								<th>Movie Release Date</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>{{ $movieDetail['movie_name'] }}</td>
								<td>{{ $movieDetail['release_date'] }}</td>
							</tr>
							</tbody>
					</table>
					
					<div class="">
						<div class=""><strong>List of actors in this movie</strong></div>
						
							@foreach($movieDetail['actors'] as $k => $actor)
								<br>
								<div class="list-group-item bg-info">{{ $actor['name'] }}</div>
									<div class="item-desc">
										@if($actor['pivot']['acting_desc'] != '')
											{{ $actor['pivot']['acting_desc'] }}
										@else
											Not added yet by actor
										@endif
									</div>
								</div>
							@endforeach
						
					</div>
				</div>
			</div>
        </div>
@endsection
