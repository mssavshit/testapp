@extends('layouts.app')

@section('content')

        <div class="col-sm-9 main">
			
			<div class="jumbotron" style="background: #fff;">
			    <h3 class="h1-responsive section-title">Movies</h3>
			    
		        	<a class="btn btn-primary btn-sm action-btn pull-right" href="{{ route('movies_create') }}">Add Movie</a>
		       
			    <hr class="my-2">
			   
				<div>
					<table class="table table-striped">
						<thead>
							<tr>
								<th>Sr. No</th>
								<th>Movie Name</th>
								<th>Movie Release Date</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							@foreach($movies as $k => $movie)
								<tr>
									<td>{{ $k+1 }}</td>
									<td>
										<a href="{{ route('movies_show', $movie->id) }}" >{{$movie->movie_name}}</a>
									</td>
									<td>
										{{ \Carbon\Carbon::parse($movie->release_date)->format('j F, Y') }}
									</td>
									<td>
										<a href="JAVACRIPT:void(0);" class="btn btn-primary btn-sm" data-toggle="collapse" data-target="#movie{{ $movie->id }}">Show Actors</a>
									</td>
								</tr>
								<tr id="movie{{ $movie->id }}" class="collapse border-right border-left border-bottom">
									<td colspan="4">
										@forelse($movie->actors as $j => $actor)
											
											<table class="table table-striped">
												<tbody>
													<tr>
														<td>{{ $j+1 }}</td>
														<td>{{ $actor->name }}</td>
														<td>{{ $actor->email }}</td>
													</tr>
												</tbody>
											</table>
										@empty
											<div class="text-center">No Actor Found</div>
										@endforelse

									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
        </div>
@endsection
