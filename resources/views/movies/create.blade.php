@extends('layouts.app')


@section('content')
	<div class="col-sm-9 main">
		<form method="POST" action="{{ route('movies_store')}}">
			<div class="jumbotron" style="background: #fff;">
			    <h3 class="h1-responsive section-title">Add Movie</h3>
			    
		        	<a class="btn btn-primary btn-sm action-btn pull-right" href="{{ route('movies_create') }}">Add Movie</a>
		       
			    <hr class="my-2">
			   
				{{ csrf_field() }}
				<div class="form-group">
					<label for="exampleInputEmail1">Movie Title</label>
					<input type="text" class="form-control"  placeholder="Movie Title" name="movie_name" value="{{ old('movie_name') }}"/>
				</div>
				<div class="form-group">
					<label for="exampleInputPassword1">Release Date</label>
					<input type="text" class="form-control" placeholder="YYYY/MM/DD" name="release_date" value="{{ old('release_date') }}"/>
				</div>

				<div class="form-group">
					<label for="select_actors">Select Actors</label>
					<select class="form-control" name="actors[]" id="select_actors" multiple="multiple">
						@foreach($actors as $k => $actor)
							<option value="{{ $actor->id }}">{{ $actor->name }}</option>
						@endforeach
					</select>
				</div>
			  	<button type="submit" class="btn btn-primary">Submit</button>
			</div>
		</form>
		@include('layouts.error')
    </div>
		
@endsection
