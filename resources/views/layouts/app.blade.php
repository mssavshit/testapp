<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
	
	@include('includes.css')
	@include('includes.script')
    
</head>
<body>
    <div id="app">
        @include('includes.nav')

        <div class="container">
	        <div class="row">
	            
	            @if(Auth::check())
			    @include('includes.sidebar')
			    @endif
	            @yield('content')

	        </div>
        </div>
    </div>
</body>
</html>
