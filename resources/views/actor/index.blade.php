@extends('layouts.app')

@section('content')

        <div class="col-sm-9 main">
			
			<div class="jumbotron" style="background: #fff;">
			    <h3 class="h1-responsive section-title">Movies</h3>
			   
		       
			    <hr class="my-2">
			   
				<div>
					<table class="table table-striped">
						<thead>
							<tr>
								<th>Sr. No</th>
								<th>Movie Name</th>
								<th>Movie Release Date</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							@foreach($actorMovies['movies'] as $k => $movie)
								<tr>
									<td>{{ $k+1 }}</td>
									<td>
										<a href="{{ route('movies_show', $movie['id']) }}" >{{$movie['movie_name']}}</a>
									</td>
									<td>
										{{ \Carbon\Carbon::parse($movie['release_date'])->format('j F, Y') }}
									</td>
									<td>
										@if($movie['pivot']['acting_desc'] != '')

											<a href="{{ route('acting_create', $movie['id']) }}" class="btn btn-primary btn-sm">Edit</a>

										@else
											<a href="{{ route('acting_create', $movie['id']) }}" class="btn btn-primary btn-sm">Write Something</a>
										@endif
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
        </div>
@endsection
