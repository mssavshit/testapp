@extends('layouts.app')


@section('content')
	<div class="col-sm-9 main">
		<form method="POST" action="{{ route('acting_store')}}">
			<div class="jumbotron" style="background: #fff;">
			    <h3 class="h1-responsive section-title">Write about role played in movie</h3>
			    <hr class="my-2">
			   
				{{ csrf_field() }}
				
				<div class="form-group">
					<label for="acting_desc">Write something about your acting in movie</label>
					<textarea class="form-control" name="acting_desc" rows="5" id="acting_desc"> 
						{{ $acting_desc }} 
					</textarea>
					<input type="hidden" name="movie_id" value="{{ $movie_id }}" />
				</div>
			  	
			  	<button type="submit" class="btn btn-primary">Submit</button>
			</div>
		</form>
		@include('layouts.error')
    </div>
		
@endsection
